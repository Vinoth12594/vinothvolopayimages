const Labels = {
    emptyResult: 'No images found',
    searchPlaceholder: 'Search your images'
}

export default Labels