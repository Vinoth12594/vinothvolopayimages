const Colors = {
    white: 'white',
    black: 'black',
    inputBg: '#191919',
    textColor: '#A2A5B9',
}

export default Colors