/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import {
    StatusBar,
    StyleSheet,
    Text,
    View,
    FlatList,
    TextInput,
    ActivityIndicator,
    RefreshControl,
    SafeAreaView
} from 'react-native';
import { PHOTOS_LIST, SEARCH, UNSPLASH_CLIENT_ID } from '../api';
import { normalize, Colors, Labels } from '../assets';
import FastImage from 'react-native-fast-image'
import { authApi } from '../api/apiConfig';

const UnSplashImages = (props) => {

    const [searchText, setSearchText] = useState('');
    const [imagesList, setImagesList] = useState([]);
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    const [isLoading, setisLoading] = useState(false);
    const [isRefreshing, setisRefreshing] = useState(false);
    const [reachEnd, setReachEnd] = useState(false);
    const [isFooterLoading, setisFooterLoading] = useState(false);

    useEffect(() => {
        getImages();
    }, [searchText])

    useEffect(() => {
        if (isRefreshing) getImages();
    }, [isRefreshing])

    const getImages = () => {
        if (searchText) {
            // if (page === 1) setisLoading(true);
            authApi.get(SEARCH, {
                params: {
                    client_id: UNSPLASH_CLIENT_ID,
                    page,
                    per_page: perPage,
                    query: searchText
                }
            })
                .then(response => {
                    handleImageResponse(response.data?.results);
                })
                .catch(({ error, responseData }) => {
                    setisLoading(false);
                    setisRefreshing(false);
                    setisFooterLoading(false);
                    setReachEnd(true);
                })
        } else {
            // if (page === 1) setisLoading(true);
            authApi.get(PHOTOS_LIST, {
                params: {
                    client_id: UNSPLASH_CLIENT_ID,
                    page,
                    per_page: perPage
                }
            })
                .then(response => {
                    console.log(response.data);
                    handleImageResponse(response.data);
                })
                .catch(({ error, responseData }) => {
                    setisLoading(false);
                    setisRefreshing(false);
                    setisFooterLoading(false);
                    setReachEnd(true);
                })
        }
    }

    const handleImageResponse = (newImagesList) => {
        if (newImagesList.length > 0) {
            if (page === 1) {
                setImagesList(newImagesList);
            } else {
                setImagesList([...imagesList, ...newImagesList]);
            }
            setPage(page + 1);
        } else {
            setReachEnd(true);
        }
        setisLoading(false);
        setisRefreshing(false);
        setisFooterLoading(false);
    }

    const EmptyComponent = () => {
        return !isLoading && <Text style={styles.emptyContainer}>{Labels.emptyResult}</Text>
    }

    const renderImage = ({ item, index }) => {
        return <FastImage
            style={styles.imageContainer}
            source={{
                uri: item?.urls?.thumb,
                priority: FastImage.priority.high
            }}
            resizeMode={FastImage.resizeMode.cover}
        />
    }

    const FooterLoader = () => {
        return isFooterLoading && <ActivityIndicator color={Colors.white} size='large' style={styles.footerLoader} />
    }

    const onPageReachEnd = () => {
        if (!reachEnd && !isLoading) {
            setisFooterLoading(true);
            getImages();
        }
    }

    const imagesListRefresh = () => {
        setPage(1);
        setReachEnd(false);
        setisRefreshing(true);
    }

    return (
        <SafeAreaView style={styles.container}>
            <StatusBar backgroundColor={Colors.black} barStyle='light-content' />

            <View style={styles.headerContainer}>
                <TextInput
                    style={styles.searchInput}
                    value={searchText}
                    onChangeText={(text) => {
                        if (page > 1) setPage(1);
                        setSearchText(text);
                    }}
                    placeholder={Labels.searchPlaceholder}
                    placeholderTextColor={Colors.textColor}
                />
            </View>
            <FlatList
                data={imagesList}
                renderItem={renderImage}
                keyExtractor={(item, index) => index.toString()}
                numColumns={2}
                refreshControl={<RefreshControl
                    colors={[Colors.white]}
                    refreshing={isRefreshing}
                    onRefresh={imagesListRefresh} />
                }
                contentContainerStyle={styles.flatContainer}
                onEndReached={onPageReachEnd}
                onEndReachedThreshold={0.2}
                ListEmptyComponent={<EmptyComponent />}
                ListFooterComponent={<FooterLoader />}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.black
    },
    headerContainer: {
        margin: normalize(12)
    },
    searchInput: {
        backgroundColor: Colors.inputBg,
        height: normalize(42),
        borderRadius: normalize(6),
        borderColor: Colors.white,
        fontSize: normalize(14),
        color: Colors.white,
        paddingHorizontal: normalize(8)
    },
    flatContainer: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    emptyContainer: {
        alignItems: 'center',
        color: Colors.white,
        fontSize: normalize(14),
        fontWeight: '600',
        textAlign: 'center'
    },
    loader: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    footerLoader: {
        marginVertical: normalize(4)
    },
    imageContainer: {
        width: normalize(160),
        height: normalize(160),
        margin: normalize(4)
    }
})

export default UnSplashImages;
