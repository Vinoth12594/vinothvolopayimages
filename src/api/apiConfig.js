import axios from 'axios';
import { BASE_URL } from './';

//Auth api
export const authApi = axios.create({
  baseURL: BASE_URL,
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Cache-Control': 'no-cache',
  },
});

authApi.defaults.params = {};
authApi.interceptors.request.use(
  async function (options) {
    console.log(options.params);
    return options;
  },
  function (error) {
    return Promise.reject(error);
  },
);

authApi.interceptors.response.use(
  async function (options) {
    // console.log(options.data);
    return options;
  },
  function (error) {
    // console.log(error?.response || error);
    return Promise.reject({error, responseData: error.response});
  },
);